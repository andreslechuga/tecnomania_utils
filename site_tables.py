import requests
import pandas as pd

#in_out = 'S'
#place = 'ON'
#num_auto = '5'
#http://localhost:5000/tables

def get_bus_data(in_out, place, num_auto):
    base = 'https://api.datik.ipanel.cloud/countdown/v1/ws_estatus_lugar?indicadorLlegadaSalida={}&vLugar={}&vNumeroAutobusesAConsultar={}'.format(in_out, place, num_auto)
    response = requests.get(base)
    bus_df = []
    for item in response.json()['varArrEstAutobus']:
    	cod_auto = item['codigoAutobus']
    	tiempo_salida = item['descripcion']
    	destino = item['ruta']
    	obs = [cod_auto, tiempo_salida, destino]
    	bus_df.append(obs)
    df = pd.DataFrame(bus_df)
    df.columns = ['cod_auto', 'SALIDA', 'DESTINO']
    return df


from flask import *
import pandas as pd
app = Flask(__name__)

@app.route("/tables")
def show_tables():
    data = get_bus_data('S','GTO','10')
    data.set_index(['cod_auto'], inplace=True)
    data.index.name=None
    return render_template('view.html',tables=[data.to_html(classes='female')],
    titles = ['na'])

if __name__ == "__main__":
    app.run(debug=True, port=8000)
